from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

# For the ExpenseCategory list, have it show a table with columns
# for the name and the number of receipts associated to that category.


class CategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


# For the Account list, have it show a table with columns
# for the name, number, and the number of receipts associated to that account.

class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
